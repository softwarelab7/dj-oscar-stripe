=============================
Django Oscar Stripe
=============================

.. image:: https://badge.fury.io/py/dj-oscar-stripe.png
    :target: https://badge.fury.io/py/dj-oscar-stripe

.. image:: https://travis-ci.org/aidzz/dj-oscar-stripe.png?branch=master
    :target: https://travis-ci.org/aidzz/dj-oscar-stripe

Currently up-to-date with Stripe API Version: 2017-04-06

Documentation
-------------

The full documentation is at https://dj-oscar-stripe.readthedocs.org.

Quickstart
----------

Install Django Oscar Stripe::

    pip install dj-oscar-stripe

Then use it in a project::

    import oscar_stripe

Features
--------

* TODO

Running Tests
--------------

Does the code actually work?

::

    source <YOURVIRTUALENV>/bin/activate
    (myenv) $ pip install -r requirements_test.txt
    (myenv) $ python runtests.py

Credits
---------

Tools used in rendering this package:

*  Cookiecutter_
*  `cookiecutter-djangopackage`_

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`cookiecutter-djangopackage`: https://github.com/pydanny/cookiecutter-djangopackage