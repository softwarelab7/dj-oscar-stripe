from django.contrib import admin
from .models import Customer, ManagedAccount, ManagedBankAccount


class CustomerAdmin(admin.ModelAdmin):
    pass


class ManagedAccountAdmin(admin.ModelAdmin):
    pass


class ManagedBankAccountAdmin(admin.ModelAdmin):
    pass


admin.site.register(Customer, CustomerAdmin)
admin.site.register(ManagedAccount, ManagedAccountAdmin)
admin.site.register(ManagedBankAccount, ManagedBankAccountAdmin)
