import logging
import stripe
from django.conf import settings
from functools import wraps

stripe.api_key = settings.STRIPE_API_KEY
logger = logging.getLogger('customstripe.gateway')


def handle_stripe(function):
    @wraps(function)
    def decorated(*args, **kwargs):
        obj = False
        try:
            obj = function(*args, **kwargs)
        except stripe.error.CardError, e:
            # The card has been declined
            logger.error("Stripe Card Error: {}".format(e))
        except stripe.error.InvalidRequestError, e:
            # Invalid parameters were supplied to Stripe's API
            logger.error("Invalid Request Error: {}".format(e))
        except stripe.error.AuthenticationError, e:
            # Authentication with Stripe's API failed
            # (maybe you changed API keys recently)
            logger.error("Authentication Error: {}".format(e))
        except stripe.error.APIConnectionError, e:
            # Network communication with Stripe failed
            logger.error("Network Communication with Stripe failed: {}".format(
                e))
        except stripe.error.StripeError, e:
            logger.error("Stripe Error: {}".format(e))
        except Exception, e:
            logger.error("Error: {}".format(e))
        if not obj:
            return (False, e)
        return (True, obj)
    return decorated
