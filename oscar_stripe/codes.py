from django.utils.translation import ugettext as _

AUTHORIZED = 'Authorized'
SETTLED = 'Settled'
PENDING = 'Pending'
REFUND = 'Refund'
DEBIT = 'Debit'
CREDIT = 'Credit'
TRANSFERRED = 'Transferred'
TRANSFER_PAID = 'Transfer Paid'
TRANSFER_FAILED = 'Transfer Failed'

SOURCE_TYPE_MAP = {
    'authorized': ('Stripe', 'CC', 'amount_allocated', AUTHORIZED),
    'settled': ('Stripe', 'CC', 'amount_debited', SETTLED),
    'refund': ('Stripe', 'CC', 'amount_refunded', REFUND),
    'debit': ('Direct', 'Cash', 'amount_debited', DEBIT),
    'credit': ('Direct', 'Cash', 'amount_refunded', CREDIT),
    'transferred': ('Stripe', 'CC', 'amount_allocated', TRANSFERRED),
    'transfer-paid': ('Stripe', 'CC', 'amount_allocated', TRANSFER_PAID),
    'transfer-failed': ('Stripe', 'CC', 'amount_allocated', TRANSFER_FAILED)
}

CHARGE = 'charge'
TRANSFER = 'transfer'

STRIPE_OBJECT_CHOICES = (
    (CHARGE, _("Charge")),
    (TRANSFER, _("Transfer")),
)
