from django.conf.urls import url

from oscar.core.application import Application

from . import views


class StripeDashboardApplication(Application):
    name = 'stripe'
    default_permissions = ['is_staff', ]
    permissions_map = _map = {
        'skills-offered': (['partner.dashboard_access']),
        'update-skills-offered': (['partner.dashboard_access']),
        'list': (['is_staff'], ['partner.dashboard_access']),
        'update': (['is_staff'], ['partner.dashboard_access']),
        'notes': (['is_staff'], ['partner.dashboard_access']),
        'detail': (['is_staff'], ['partner.dashboard_access']),
        'rating-list': (['is_staff'], ['partner.dashboard_access']),
    }
    list_view = views.ManagedAccountListView
    detail_view = views.ManagedAccountDetailView
    update_view = views.ManagedAccountUpdateView
    delete_view = views.ManagedAccountDeleteView
    notes_view = views.ManagedAccountNotesView

    def get_urls(self):
        urlpatterns = [
            url(r'^$', self.list_view.as_view(), name='list'),
            url(r'^(?P<pk>\d+)/$', self.detail_view.as_view(),
                name='detail'),
            url(r'^(?P<pk>\d+)/update/$', self.update_view.as_view(),
                name='update'),
            url(r'^(?P<pk>\d+)/notes/$', self.notes_view.as_view(),
                name='notes'),
            url(r'^(?P<pk>\d+)/delete/$', self.delete_view.as_view(),
                name='delete'),
        ]
        return self.post_process_urls(urlpatterns)


application = StripeDashboardApplication()
