# stripe charges a fee composed of a percentage of the transaction plus a 
# constant amount per transaction

# percentage per transaction (in percentage)
STRIPE_FEE_PERCENT = 2.9

# constant per transaction (in USD)
STRIPE_FEE_CONSTANT = 0.3
