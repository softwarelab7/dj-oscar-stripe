import logging
import stripe

from django.conf import settings

from oscar.apps.payment.models import Source, SourceType

from .codes import SOURCE_TYPE_MAP
from .decorators import handle_stripe

logger = logging.getLogger('customstripe.gateway')
stripe.api_key = settings.STRIPE_API_KEY
stripe.api_version = settings.STRIPE_API_VERSION


@handle_stripe
def charge(token, amount, currency, order_number):
    charge = stripe.Charge.create(
        amount=amount,
        currency=currency,
        source=token,
        description=order_number
    )
    return charge


@handle_stripe
def charge_with_destination(customer_id, amount, currency, description, account_id):
    charge = stripe.Charge.create(
        amount=amount,
        currency=currency,
        customer=customer_id,
        description=description,
        destination=account_id
    )
    return charge


@handle_stripe
def charge_card(card_id, customer_id, amount, currency, order_number):
    charge = stripe.Charge.create(
        amount=amount,
        currency=currency,
        card=card_id,
        description=order_number,
        customer=customer_id,
    )
    return charge


@handle_stripe
def charge_customer(customer_id, amount, currency, description):
    charge = stripe.Charge.create(
        amount=amount,
        currency=currency,
        description=description,
        customer=customer_id,
    )
    return charge


def charge_bankcard(bankcard, billing_address, amount, currency, order_number):
    # build source from bankcard and billing address
    source = {
        'object': "card",
        'number': bankcard.number,
        'exp_month': bankcard.expiry_date.month,
        'exp_year': bankcard.expiry_date.year,
        'cvc': bankcard.ccv,
        'name': "{} {}".format(billing_address['first_name'],
                               billing_address['last_name']),
        'address_line1': billing_address['line1'],
        'address_line2': billing_address['line2'],
        'address_city': billing_address['line4'],
        'address_zip': billing_address['postcode'],
        'address_state': billing_address['state'],
        'address_country': billing_address['country']
    }
    return _charge_bankcard(source, amount, currency, order_number)


@handle_stripe
def _charge_bankcard(source, amount, currency, order_number):
    charge = stripe.Charge.create(
        amount=amount,
        currency=currency,
        source=source,
        description=order_number,
        capture=settings.STRIPE_CAPTURE
    )
    return charge


def refund(charge_id, amount):
    # get Stripe charge
    (success, charge) = retrieve_charge(charge_id)
    if success:
        return _refund(charge, amount)
    else:
        return (success, charge)


@handle_stripe
def _refund(charge, amount):
    refund = charge.refunds.create(amount=amount)
    return refund


@handle_stripe
def retrieve_charge(charge_id):
    charge = stripe.Charge.retrieve(charge_id)
    return charge


@handle_stripe
def transfer(stripe_id, charge_id, amount, description, currency='usd'):
    transfer = stripe.Transfer.create(
        amount=amount,
        currency=currency,
        destination=stripe_id,
        source_transaction=charge_id,
        metadata={'descrtiption': description}
    )
    return transfer


@handle_stripe
def payout(amount, description, currency='usd'):
    payout = stripe.Payout.create(
        amount=amount,
        currency=currency,
        metadata={'description': description}
    )
    return payout


@handle_stripe
def create(user, birthdate, token, ip_address, date, country='US'):
    account = stripe.Account.create(
        managed=True,
        country=country,
        email=user.email,
        tos_acceptance={
            'ip': ip_address,
            'date': date,
        },
        legal_entity={
            'type': 'individual',
            'first_name': user.first_name,
            'last_name': user.last_name,
            'dob': {
                "day": birthdate.day,
                "month": birthdate.month,
                "year": birthdate.year
            }
        },
        external_account=token
    )
    return account


@handle_stripe
def create_from_account_details(user, birthdate, routing_num, account_num,
                                ip_address, date, country='US', currency='USD'):
    account = stripe.Account.create(
        managed=True,
        country=country,
        email=user.email,
        tos_acceptance={
            'ip': ip_address,
            'date': date,
        },
        legal_entity={
            'type': 'individual',
            'first_name': user.first_name,
            'last_name': user.last_name,
            'dob': {
                "day": birthdate.day,
                "month": birthdate.month,
                "year": birthdate.year
            }
        },
        external_account={
            'object': 'bank_account',
            'country': country,
            'currency': currency,
            'routing_number': routing_num,
            'account_number': account_num,
        }
    )
    return account


def create_transaction(event_code, order, amount, reference):
    # Record payment source and create transaction
    (name, txn_type, amount_key, status) = SOURCE_TYPE_MAP[event_code]
    source_type, is_created = SourceType.objects.get_or_create(
        name=name)
    kwargs = {
        'source_type': source_type,
        'currency': order.currency,
        'order': order,
        amount_key: amount
    }
    source_temp = Source(**kwargs)
    source, is_created = source_type.sources.get_or_create(order=order)
    source.amount_debited += source_temp.amount_debited
    source.amount_refunded += source_temp.amount_refunded
    source.save()
    source.transactions.create(txn_type=txn_type, reference=reference,
                               amount=amount, status=status)


@handle_stripe
def create_customer(email):
    customer = stripe.Customer.create(email=email)
    return customer


@handle_stripe
def save_card(customer, token):
    card = customer.sources.create(source=token)
    return card


@handle_stripe
def retrieve_account(account_id):
    account = stripe.Account.retrieve(account_id)
    return account


@handle_stripe
def delete_account(account):
    deleted = account.delete()
    return deleted


@handle_stripe
def retrieve_customer(customer_id):
    customer = stripe.Customer.retrieve(customer_id)
    return customer


@handle_stripe
def add_bank_account(account, token, default=True):
    bank_acct = account.external_accounts.create(
        external_account=token,
        default_for_currency=default
    )
    return bank_acct


@handle_stripe
def delete_card(customer, source_id):
    card = customer.sources.retrieve(source_id).delete()
    return card


@handle_stripe
def upload_file(file, account_id):
    token = stripe.FileUpload.create(
        purpose="identity_document",
        file=file,
        stripe_account=account_id
    )
    return token


def update_default_card(customer_id, source_id):
    customer = False
    try:
        customer = stripe.Customer.retrieve(customer_id)
        customer.default_source = source_id
        customer.save()
    except stripe.error.InvalidRequestError, e:
        # Invalid parameters were supplied to Stripe's API
        logger.error("Invalid Request Error: {}".format(e))
        customer = False
    except stripe.error.AuthenticationError, e:
        # Authentication with Stripe's API failed
        logger.error("Authentication Error: {}".format(e))
        customer = False
    except stripe.error.APIConnectionError, e:
        # Network communication with Stripe failed
        logger.error("Network Communication with Stripe failed: {}".format(
            e))
        customer = False
    except stripe.error.StripeError, e:
        logger.error("Stripe Error: {}".format(e))
        customer = False
    except Exception, e:
        logger.error("Error: {}".format(e))
        customer = False
    if not customer:
        return (False, e)
    return (True, customer)


def update_bank_account(stripe_id, bank_account_id, default=True):
    # can only change which bank account is default when updating
    bank_account = False
    try:
        account = stripe.Account.retrieve(stripe_id)
        bank_account = account.external_accounts.retrieve(bank_account_id)
        bank_account.default_for_currency = default
        bank_account.save()
    except stripe.error.InvalidRequestError, e:
        # Invalid parameters were supplied to Stripe's API
        logger.error("Invalid Request Error: {}".format(e))
        bank_account = False
    except stripe.error.AuthenticationError, e:
        # Authentication with Stripe's API failed
        logger.error("Authentication Error: {}".format(e))
        bank_account = False
    except stripe.error.APIConnectionError, e:
        # Network communication with Stripe failed
        logger.error("Network Communication with Stripe failed: {}".format(
            e))
        bank_account = False
    except stripe.error.StripeError, e:
        logger.error("Stripe Error: {}".format(e))
        bank_account = False
    except Exception, e:
        logger.error("Error: {}".format(e))
        bank_account = False
    if not bank_account:
        return (False, e)
    return (True, bank_account)


def change_bank_account(stripe_id, routing_num, account_num, country='US',
                        currency='usd'):
    account = False
    try:
        account = stripe.Account.retrieve(stripe_id)
        account.external_account = {
            'object': 'bank_account',
            'country': country,
            'currency': currency,
            'routing_number': routing_num,
            'account_number': account_num,
        }
        account.save()
    except stripe.error.InvalidRequestError, e:
        # Invalid parameters were supplied to Stripe's API
        logger.error("Invalid Request Error: {}".format(e))
        account = False
    except stripe.error.AuthenticationError, e:
        # Authentication with Stripe's API failed
        logger.error("Authentication Error: {}".format(e))
        account = False
    except stripe.error.APIConnectionError, e:
        # Network communication with Stripe failed
        logger.error("Network Communication with Stripe failed: {}".format(
            e))
        account = False
    except stripe.error.StripeError, e:
        logger.error("Stripe Error: {}".format(e))
        account = False
    except Exception, e:
        logger.error("Error: {}".format(e))
        account = False
    if not account:
        return (False, e)
    return (True, account)


@handle_stripe
def get_balance():
    return stripe.Balance.retrieve()
