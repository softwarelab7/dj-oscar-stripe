import calendar
import datetime
from decimal import Decimal as D

from oscar.core.utils import get_default_currency

from . import gateway
from .models import Customer


def get_stripe_customer(user):
    """
    gets or creates customer object for user
    if created, create stripe customer on stripe gateway
    if not created, retrieve stripe customer on stripe gateway
    if stripe customer cannot be retrieved, create stripe customer and
    update customer object
    return value is stripe customer
    """
    customer, created = Customer.objects.get_or_create(user=user)
    if not created:
        success, return_value = gateway.retrieve_customer(customer.customer_id)
        if success:
            return (True, return_value)
    success, return_value = gateway.create_customer(customer.user.email)
    if success:
        customer.customer_id = return_value.get('id')
        customer.save()
        return (True, return_value)
    else:
        return (False, return_value)


def get_expiry_date_from_stripe_card(card):
    exp_year = card['exp_year']
    exp_month = card['exp_month']
    day = calendar.monthrange(exp_year, exp_month)[1]
    return datetime.date(exp_year, exp_month, day)


def get_kind_of_balance(balance, kind):
    """
    balance input should be return_value of gateway get_balance
    returns balance (kind can be 'available', or 'pending')
    in stripe account in Decimal
    """
    kind_list = balance.get(kind)
    kind_in_default_currency = next((x for x in kind_list if x["currency"] == get_default_currency().lower()), None)
    if kind_in_default_currency:
        return D(kind_in_default_currency.get('amount', 0))
    else:
        return D(0.0)
