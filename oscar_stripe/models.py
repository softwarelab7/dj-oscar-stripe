from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.fields import UUIDField

from oscar.core.compat import AUTH_USER_MODEL
from oscar.core.utils import get_default_currency

from .codes import STRIPE_OBJECT_CHOICES


class Customer(models.Model):
    """
    Model to represent customer. customer_id is the stripe customer id
    """
    user = models.OneToOneField(AUTH_USER_MODEL, related_name='customer',
                                verbose_name=_("User"))
    customer_id = models.CharField(max_length=255)

    def __unicode__(self):
        return unicode(_(u"{} <{}> - {}".format(
            self.user.get_full_name(), self.user.email, self.customer_id)))


class ManagedAccount(models.Model):
    """
    Stripe managed account
    """
    stripe_id = models.CharField(max_length=255, unique=True)
    user = models.OneToOneField(AUTH_USER_MODEL)
    date_created = models.DateTimeField(_('Date Created'), auto_now_add=True)
    last_updated = models.DateTimeField(_('Last Updated'), auto_now=True)
    # True if verification is needed, fields_needed shows the requirements for
    # verification
    verification = models.BooleanField(default=False)
    fields_needed = models.TextField()
    updated_json = models.TextField(null=True, blank=True)
    due_by = models.DateTimeField(null=True, blank=True)
    uuid = UUIDField(unique=True)

    def get_full_name(self):
        full_name = _("{} {}".format(self.user.first_name, self.user.last_name))
        return full_name.strip()

    def __unicode__(self):
        return unicode(_(u"{} <{}> - {}".format(
            self.get_full_name(), self.user.email, self.stripe_id)))


class ManagedAccountNote(models.Model):
    account = models.ForeignKey('oscar_stripe.ManagedAccount', related_name='notes',
                                verbose_name=_('Managed Account'))
    user = models.ForeignKey(AUTH_USER_MODEL, verbose_name=_("User"))
    message = models.TextField(_("Message"))
    date_created = models.DateTimeField(_("Date Created"), auto_now_add=True)
    date_updated = models.DateTimeField(_("Date Updated"), auto_now=True)


class ManagedBankAccount(models.Model):
    """
    Bank account/s linked to a managed Stripe account
    """
    stripe_id = models.CharField(max_length=255, unique=True)
    account = models.ForeignKey('oscar_stripe.ManagedAccount',
                                related_name='bank_accounts')
    default = models.BooleanField(default=False)
    last4 = models.CharField(max_length=4)
    uuid = UUIDField(unique=True)

    def __unicode__(self):
        return unicode(_(u"{} - {}".format(
            self.account, self.stripe_id)))
