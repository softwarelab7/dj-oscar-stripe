import logging
from django import forms
from django.utils.translation import ugettext_lazy as _

logger = logging.getLogger(__name__)


class ManagedAccountForm(forms.Form):
    line1 = forms.CharField(label=_("First line of address"), max_length=255)
    city = forms.CharField(label=_("City"), max_length=255)
    state = forms.CharField(label=_("State/County"), max_length=255)
    postcode = forms.CharField(
        label=_("Post/Zip-code"), max_length=64)
    last4 = forms.CharField(help_text=_("You can either provide the last 4 digits of your SSN or your entire SSN, but not both"),
                            label=_("Last 4 Digits of SSN"), max_length=4, required=False)
    ssn = forms.CharField(
        label=_("SSN"), max_length=11, required=False)
    token = forms.CharField(max_length=256, required=False, widget=forms.HiddenInput())
    supporting = forms.FileField(label=_("Supporting Document"), required=False)

    def clean(self):
        last4 = self.cleaned_data.get('last4', None)
        token = self.cleaned_data.get('token', None)
        if (last4 or token):
            if not bool(last4) ^ bool(token):
                raise forms.ValidationError(_("Please provide only one of the following: 'Last 4 Digits of SSN' or 'SSN'."))
        else:
            raise forms.ValidationError(_("Please provide either one of the following: 'Last 4 Digits of SSN' or 'SSN'."))
        return self.cleaned_data
