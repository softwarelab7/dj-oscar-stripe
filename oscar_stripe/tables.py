from oscar.apps.dashboard.tables import DashboardTable

from oscar_stripe.models import ManagedAccount


class ManagedAccountTable(DashboardTable):
    class Meta(DashboardTable.Meta):
        model = ManagedAccount
        fields = ('user', 'verification', 'fields_needed', 'due_by',
                  'last_updated')
        order_by = '-last_updated'
