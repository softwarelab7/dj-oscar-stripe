import logging
from django.conf import settings
from django.contrib import messages
from django.core.files.base import ContentFile
from django.core.urlresolvers import reverse
from django.shortcuts import redirect
from django.utils.translation import ugettext_lazy as _
from django.views.generic import DeleteView, DetailView, FormView, ListView
from django.views.generic.detail import SingleObjectMixin
from django_tables2 import SingleTableMixin

from . import gateway
from .forms import ManagedAccountForm
from .models import ManagedAccount, ManagedAccountNote
from .tables import ManagedAccountTable

logger = logging.getLogger(__name__)


class ManagedAccountListView(SingleTableMixin, ListView):
    context_object_name = 'accounts'
    model = ManagedAccount
    # template_name = 'stylists/stylist_list.html'
    table_class = ManagedAccountTable
    context_table_name = 'accounts'

    def get_queryset(self):
        if self.request.user.is_staff:
            queryset = ManagedAccount.objects.all()
        else:
            queryset = ManagedAccount.objects.filter(user=self.request.user)
        return queryset

    def get_context_data(self, **kwargs):
        ctx = super(ManagedAccountListView, self).get_context_data(**kwargs)
        ctx['title'] = _('Managed Accounts')
        return ctx

    def get_table(self, **kwargs):
        table = super(ManagedAccountListView, self).get_table(**kwargs)
        table.caption = _('Managed Accounts')
        return table


class ManagedAccountDetailView(DetailView):
    context_object_name = 'stripe_account'
    model = ManagedAccount

    def get_queryset(self):
        if self.request.user.is_staff:
            queryset = ManagedAccount.objects.all()
        else:
            queryset = ManagedAccount.objects.filter(user=self.request.user)
        return queryset

    def get_object(self):
        obj = super(ManagedAccountDetailView, self).get_object()
        self.account = obj
        self.username = obj.user.get_full_name() if obj.user.get_full_name() else obj.user.email
        success, return_value = gateway.retrieve_account(obj.stripe_id)
        if not success:
            messages.error(self.request, _(u"Error retrieving stripe account details: '{}'".format(return_value)),
                extra_tags='danger')
            return None
        else:
            return return_value

    def get_context_data(self, **kwargs):
        ctx = super(ManagedAccountDetailView, self).get_context_data(**kwargs)
        ctx['title'] = _(u'Managed Account for {}'.format(self.username))
        ctx['account'] = self.account
        return ctx


class ManagedAccountUpdateView(SingleObjectMixin, FormView):
    form_class = ManagedAccountForm
    template_name = 'oscar_stripe/managedaccount_form.html'
    model = ManagedAccount

    def get_context_data(self, **kwargs):
        self.object = self.get_object()
        ctx = super(ManagedAccountUpdateView, self).get_context_data(**kwargs)
        ctx['title'] = _(u"Update Stripe Account information for '{}'".format(
            self.object.get_full_name()))
        ctx['publishable_key'] = settings.STRIPE_PUBLISHABLE_API_KEY
        return ctx

    def get_queryset(self):
        if self.request.user.is_staff:
            return ManagedAccount.objects.all()
        else:
            return ManagedAccount.objects.filter(user=self.request.user)

    def get_form_kwargs(self):
        kwargs = super(ManagedAccountUpdateView, self).get_form_kwargs()
        self.object = self.get_object()
        return kwargs

    def get_success_url(self):
        messages.success(
            self.request,
            _(u"Stripe Account Info for '{}' was updated successfully.".format(
                self.object.get_full_name()))
        )
        return reverse('stripe:list')

    def update_stripe_account(self, data):
        stripe_id = self.object.stripe_id
        line1 = data.get('line1', None)
        city = data.get('city', None)
        state = data.get('state', None)
        postcode = data.get('postcode', None)
        last4 = data.get('last4', None)
        token = data.get('token', None)
        supporting = data.get('supporting', None)

        success, return_value = gateway.retrieve_account(stripe_id)
        if not success:
            messages.error(self.request, _(u"Error: {}".format(return_value)),
                extra_tags='danger')
            return False
        else:
            account = return_value
        if account.legal_entity.verification.status == 'verified':
            messages.error(self.request, _("Account is already verified"),
                extra_rags='danger')
            return account
        document = None
        if supporting:
            success, return_value = gateway.upload_file(ContentFile(supporting.read()), stripe_id)
            if not success:
                messages.error(
                    self.request, _(u"Error Uploading File: {}".format(return_value)),
                    extra_tags='danger')
                return False
            else:
                document = return_value.id
        account.legal_entity.address.line1 = line1
        account.legal_entity.address.city = city
        account.legal_entity.address.state = state
        account.legal_entity.address.postal_code = postcode
        account.legal_entity.address.state = state
        if last4:
            account.legal_entity.ssn_last_4 = last4
        if token:
            account.legal_entity.personal_id_number = token
        if document:
            account.legal_entity.verification.document = document
        try:
            account.save()
        except Exception as e:
            logger.error(u"Error: {}".format(e))
            messages.error(self.request, _(u"Error: {}".format(e)), extra_tags='danger')
            return False
        updated = {
            'line1': line1,
            'city': city,
            'state': state,
            'postcode': postcode,
            'last4': last4,
        }
        self.create_note(updated)
        return account

    def form_valid(self, form):
        account = self.update_stripe_account(form.cleaned_data)
        if not account:
            return self.form_invalid(form)
        stylist, _ = ManagedAccount.objects.get_or_create(
            stripe_id=self.object.stripe_id
        )
        stylist.fields_needed = ", ".join(account.verification.fields_needed)
        stylist.save()
        return redirect(self.get_success_url())

    def create_note(self, updated):
        note = _(u"{} updated the following: ".format(self.request.user.get_full_name()))
        for key, item in updated.iteritems():
            note = _(u"{} {} - {},".format(note, key, item))
        ManagedAccountNote.objects.create(
            account=self.object, user=self.request.user, message=note)


class ManagedAccountDeleteView(DeleteView):
    # template_name = 'stylists/stylist_delete.html'
    model = ManagedAccount
    context_object_name = 'account'

    def get_context_data(self, **kwargs):
        ctx = super(ManagedAccountDeleteView, self).get_context_data(**kwargs)
        user = self.get_object().user
        ctx['title'] = _("Delete managed account?")
        ctx['username'] = user.get_full_name() if user.get_full_name() else user.email
        return ctx

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        success, return_value = gateway.retrieve_account(self.object.stripe_id)
        if not success:
            messages.error(self.request, _(u"Error retrieving stripe account: '{}'".format(return_value)),
                extra_tags='danger')
            return None
        else:
            account = return_value
        success, return_value = gateway.delete_account(account)
        if not success:
            messages.error(self.request, _(u"Error deleting stripe account: '{}'".format(return_value)),
                extra_tags='danger')
            return None
        success_url = self.get_success_url()
        self.object.delete()
        return redirect(success_url)

    def get_success_url(self):
        msg = _(u"Deleted Managed Account for '{}'".format(
            self.object.get_full_name()))
        messages.success(self.request, msg)
        return reverse('stylists:list')


class ManagedAccountNotesView(DetailView):
    template_name = 'oscar_stripe/managedaccount_notes.html'
    model = ManagedAccount
    context_object_name = 'account'

    def get_queryset(self):
        if self.request.user.is_staff:
            queryset = ManagedAccount.objects.all()
        else:
            queryset = ManagedAccount.objects.filter(user=self.request.user)
        return queryset

    def get_context_data(self, **kwargs):
        ctx = super(ManagedAccountNotesView, self).get_context_data(**kwargs)
        user = self.object.user
        username = user.get_full_name() if user.get_full_name() else user.email
        ctx['title'] = _(u"Notes for Managed Account for user: {}".format(username))
        ctx['notes'] = self.object.notes.all()
        return ctx
