============
Installation
============

At the command line::

    $ easy_install dj-oscar-stripe

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv dj-oscar-stripe
    $ pip install dj-oscar-stripe
